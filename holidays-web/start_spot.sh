export max_price=0.02
export x86_ami_id=ami-0119aa4d67e59007c # t2.medium (x86 64-bit)
export arm_ami_id=ami-077007384e83bf4cc # a1.medium (arm 64-bit)
export instance_type=a1.medium

export key_name=aws-holidays-run
export security_groups=base-server
export iam_role_arn=arn:aws:iam::646970238185:instance-profile/dns-editor

export ami_id=${arm_ami_id}

export dry=--no-dry-run

aws ec2 request-spot-instances \
	${dry} \
	--spot-price ${max_price} \
	--instance-count 1 \
	--launch-specification \
		" { \
			\"ImageId\":\"${ami_id}\", \
			\"InstanceType\":\"${instance_type}\", \
        	\"KeyName\":\"${key_name}\", \
        	\"SecurityGroups\": [\"${security_groups}\"], \
			\"IamInstanceProfile\": { \
				\"Arn\": \"${iam_role_arn}\" \
			},\
        	\"UserData\":\"`cat scripts/*.sh | base64`\" \
		}"
