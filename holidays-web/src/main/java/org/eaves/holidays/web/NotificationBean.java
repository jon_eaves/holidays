package org.eaves.holidays.web;

import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.time.LocalTime;

@Service
public class NotificationBean implements Serializable {

    public String getMessage(String value) {
        return "Button was clicked at " + LocalTime.now() + " by " + value;
    }
}
