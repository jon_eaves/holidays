package org.eaves.holidays.web;

import org.eaves.holidays.data.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ElectionApplicationData implements ApplicationData {
    private static final String RESULTS = "HouseFirstPrefsByCandidateByVoteTypeDownload-24310.csv";
    private static final String LOCATIONS = "GeneralPollingPlacesDownload-24310.csv";

    private final Election _election;
    private final LocationIndex _locations;

    public ElectionApplicationData() {
        ElectionResults r = new CSVNationalElectionResultsReader(RESULTS, "Federal 2019");
        _election = r.load();
        LocationsInElectoratesReader lr = new LocationsInElectoratesReader(LOCATIONS);
        _locations = lr.load();
    }

    public Election election() {
        System.out.println(LocalDateTime.now() +  " creating global election");
        return _election;
    }

    public LocationIndex locations() {
        System.out.println(LocalDateTime.now() + " creating global locations");
        return _locations;
    }
}
