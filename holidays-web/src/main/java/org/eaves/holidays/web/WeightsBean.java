package org.eaves.holidays.web;

import org.eaves.holidays.scoring.Scoring;
import org.eaves.holidays.scoring.ScoringWeightByParty;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;
import java.util.HashMap;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class WeightsBean implements Serializable {
    
    private ScoringWeightByParty _weights = new ScoringWeightByParty(new HashMap<>(), 5);
    
    public ScoringWeightByParty weights() {
        return _weights;
    }
    
    public void update(String abbreviation, int score) {
        _weights.update(abbreviation, score);
    }
}
