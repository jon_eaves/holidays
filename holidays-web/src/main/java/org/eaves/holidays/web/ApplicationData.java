package org.eaves.holidays.web;

import org.eaves.holidays.data.Election;
import org.eaves.holidays.data.LocationIndex;
import org.eaves.holidays.scoring.ScoringWeightByParty;

public interface ApplicationData {
    Election election();

    LocationIndex locations();
}
