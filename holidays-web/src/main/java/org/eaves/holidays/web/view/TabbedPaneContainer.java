package org.eaves.holidays.web.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TabbedPaneContainer {

    private Map<Tab, Component> _tabsToPages = new HashMap<>();
    private Tabs _tabs = new Tabs();
    private Div _pages = new Div();
    private Set<Component> _shown;

    public TabbedPaneContainer() {
        _tabs.setWidthFull();
        _pages.setWidthFull();
    }

    public void add(String heading, String body) {
        Div b = new Div();
        b.setText(body);

        add(heading, b);
    }

    public void add(String heading, Component body) {
        Tab t = new Tab(heading);
        Div b = new Div(body);
        b.setVisible(false);

        _tabsToPages.put(t, b);
        _tabs.add(t);
        _pages.add(b);
        _shown = Stream.of(new Div()).collect(Collectors.toSet());

    }


    private void setupListener() {
        _tabs.addSelectedChangeListener(event -> {
            hideShowPages();
            // Notification.show("changed tab");
        });
    }

    private void hideShowPages() {
        _shown.forEach(page -> page.setVisible(false));
        _shown.clear();
        displayBodyContent();
    }

    private void showTab(int index) {
        _tabs.setSelectedIndex(index);
        displayBodyContent();
    }

    private void displayBodyContent() {
        Component selectedPage = _tabsToPages.get(_tabs.getSelectedTab());
        selectedPage.setVisible(true);
        _shown.add(selectedPage);
    }

    public void register(VerticalLayout pageView, int startTab) {
        pageView.add(_tabs);
        pageView.add(_pages);

        showTab(startTab);
        setupListener();
    }
}
