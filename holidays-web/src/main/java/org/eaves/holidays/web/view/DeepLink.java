package org.eaves.holidays.web.view;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;

@Route(value = "link")
public class DeepLink extends VerticalLayout implements BeforeEnterObserver
{
    @Override
    public void beforeEnter(final BeforeEnterEvent event) {
        String code = event.getLocation().getQueryParameters().getParameters().get("tab").get(0);

        event.getUI().getSession().setAttribute("tab", code);
        event.rerouteTo(HomePageView.class);
    }        
}
