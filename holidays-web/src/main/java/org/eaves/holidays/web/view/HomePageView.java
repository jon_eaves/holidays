package org.eaves.holidays.web.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.router.Route;
import org.eaves.holidays.data.*;
import org.eaves.holidays.scoring.ScoringWeightByParty;
import org.eaves.holidays.web.ApplicationData;
import org.eaves.holidays.web.WeightsBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Route("")
/* @PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base") */
public class HomePageView extends VerticalLayout {

    private int _totalVotes;
        private final boolean _debugBorder = false;

    public HomePageView(final ApplicationData data, final WeightsBean weights) {
        /* Set up the tabs */

        Election e = data.election();
        LocationIndex l = data.locations();

        TabbedPaneContainer tbc = new TabbedPaneContainer();

        VerticalLayout searchBySuburbPanel = searchBySuburbPanel(e, l);
        tbc.add("Find Electorate by Suburb", searchBySuburbPanel);

        VerticalLayout searchByCandidatePanel = searchByCandidatePanel(e);
        tbc.add("Find Electorate by Candidate", searchByCandidatePanel);

        VerticalLayout electorateDetails = showElectoratePanel(e, l);
        tbc.add("Electorate Details", electorateDetails);

        VerticalLayout partyScoringPanel = holidayLocationScoringPanel(e, weights);
        tbc.add("Holiday Locations", partyScoringPanel);

        VerticalLayout aboutPanel = aboutPanel();

        String tab = (String) UI.getCurrent().getSession().getAttribute("tab");
        int startTab = 0;
        if (tab != null) {
            startTab = Integer.parseInt(tab);
        }

        tbc.add("About", aboutPanel);
        tbc.register(this, startTab);
    }

    private VerticalLayout showElectoratePanel(Election e, LocationIndex l) {
        VerticalLayout panel = new VerticalLayout();
        
        HorizontalLayout mini = new HorizontalLayout();
        
        ComboBox<Location> cb = new ComboBox<>();
        cb.setItemLabelGenerator(Location::toString);
        cb.setPlaceholder("type electorate name");
        cb.setItems(l.allElectorates().stream().distinct().sorted());
        cb.setClearButtonVisible(true);

        mini.add(cb);
        mini.setAlignSelf(Alignment.START);
        mini.setSpacing(true);

        mini.add(new Div());
        mini.add("These are not suburbs, but electorates");

        Div body = new Div();
        body.setText("Nothing selected yet");

        HorizontalLayout dualGrid = new HorizontalLayout();
        dualGrid.setSizeFull();
        border(dualGrid, "2pt solid orange");
        
        Grid<Candidate> candidates = new Grid<>(Candidate.class);
        candidates.addColumn(Candidate::name).setHeader("Name");
        candidates.addColumn(Candidate::partyName).setHeader("Party");
        candidates.addColumn(Candidate::votes).setHeader("Votes");
        candidates.addColumn(c -> String.format("%.2f", (float) c.votes() / (float) _totalVotes * 100.0)).setHeader("Percentage");
        candidates.setVisible(false);

        Grid<Location> pollingPlaces = new Grid<>();
        pollingPlaces.addColumn(Location::toString).setHeader("Suburbs");
        pollingPlaces.setVisible(false);

        Button button = new Button("Show Electorate");
        button.addClickListener(event -> {
            body.setText("No search results");
            if (cb.getValue() != null) {
                candidates.setVisible(false);
                pollingPlaces.setVisible(false);

                String text = String.valueOf(cb.getValue());
                Notification.show(text);

                Location electorate = cb.getValue();

                Seat seat = e.contest(electorate);
                body.setText("The electorate " + text);
                candidates.setItems(seat.candidates());
                _totalVotes = seat.totalVotes();
                candidates.getColumns()
                        .forEach(c -> c.setAutoWidth(true));
                candidates.recalculateColumnWidths();
                candidates.setWidth("65%");
                candidates.setVisible(true);
                
                pollingPlaces.setItems(l.suburbsInElectorate(cb.getValue()).stream().sorted());
                pollingPlaces.getColumns().forEach(c -> c.setAutoWidth(true));
                pollingPlaces.recalculateColumnWidths();
                pollingPlaces.setWidth("35%");
                pollingPlaces.setVisible(true);
            }
        });

        dualGrid.add(candidates);
        dualGrid.add(pollingPlaces);
        
        panel.add(mini);
        panel.add(button);

        panel.add(body);
        panel.add(dualGrid);
        return panel;
    }

    private VerticalLayout aboutPanel() {
        VerticalLayout panel = new VerticalLayout();

        String output = "Where I want to spend my money on family holidays." +
                "<p/><p/>" +
                "Data from AEC, GeneralPollingPlacesDownload-24310.csv (2019 Federal Election)" +
                "<br/>" + "and HouseFirstPrefsByCandidateByVoteTypeDownload-24310.csv (2019 Federal Election)";

        Div d = new Div();
        d.getElement().setProperty("innerHTML", output);

        panel.add(d);
        return panel;
    }

    private void border(Component c, String style) {
        if (_debugBorder) c.getElement().getStyle().set("border", style);
    }

    private VerticalLayout holidayLocationScoringPanel(Election e, WeightsBean weights) {
        VerticalLayout panel = new VerticalLayout();

        Div usage = new Div();
        usage.setText("To use the location finder, select the party from the list, type a number " +
        "into the box and click the \"Change Weighting\" button.  At any time, click the \"Top 10\" button " +
                "to get the list of electorates that best match your weighting");
        
        border(panel, "6pt dotted DarkOrange");

        HorizontalLayout dualPanes = new HorizontalLayout();
        border(dualPanes, "2px solid Black");
        dualPanes.setSizeFull();

        VerticalLayout selectPane = new VerticalLayout();
        border(selectPane, "2px solid Pink");

        Grid<Party> parties = new Grid<>(Party.class);
        parties.addColumn(Party::abbreviation).setHeader("Abbreviation").setFlexGrow(1);
        parties.addColumn(Party::name).setHeader("Name").setFlexGrow(4);
        parties.addColumn(c -> {
            int w = weights.weights().find(c.abbreviation());
            return String.format("%d", w);
        }).setHeader("Weight").setFlexGrow(1);
        parties.setWidth("100%");
        parties.setItems(e.parties());
        parties.recalculateColumnWidths();

        NumberField weightingValue = new NumberField();
        weightingValue.setValue(5d);

        Button button = new Button("Change Weighting");
        button.addClickListener(event -> {
            Set<Party> selected = parties.getSelectedItems();
            if (selected != null && !selected.isEmpty() && !weightingValue.isEmpty()) {
                Party p = selected.iterator().next();
                int newValue = weightingValue.getValue().intValue();
                String text = p.name() + " " + newValue;
                notify(text);
                weights.update(p.abbreviation(), newValue);
                parties.getDataProvider().refreshAll();
            }
        });

        selectPane.add(parties);

        HorizontalLayout h = new HorizontalLayout();
        
        h.add(button);
        h.add(new Div());
        h.add(weightingValue);

        selectPane.add(h);
        dualPanes.add(selectPane);

        VerticalLayout showPane = new VerticalLayout();
        Grid<Scored> top = new Grid<>(Scored.class);
        top.addColumn(Scored::score).setHeader("Score");
        top.addColumn(Scored::location).setHeader("Electorate");

        showPane.add(top);
        Button top10 = new Button("Top 10");
        top10.addClickListener(event -> {
            ScoringWeightByParty w = weights.weights();
            top.setItems(top(e, w, 10));
            // top.setHeightByRows(true);
            top.setHeight("10");
        });
        showPane.add(top10);
        border(showPane, "2pt solid blue");
        dualPanes.add(showPane);

        panel.add(dualPanes);
        panel.add(usage);
        return panel;
    }

    private void notify(String text) {
        Notification n = new Notification(text, 1000);
        n.open();
    }

    public List<Scored> top(Election e, ScoringWeightByParty weights, int number) {
        Scored[] s;
        s = e.score(weights.scoreWeights(), 0);

        int size = s.length;

        return new ArrayList<>(Arrays.asList(s).subList(0, (Math.min(number, size))));
    }

    private VerticalLayout searchBySuburbPanel(Election e, LocationIndex l) {
        VerticalLayout panel = new VerticalLayout();

        HorizontalLayout mini = new HorizontalLayout();

        ComboBox<Location> cb = new ComboBox<>();
        cb.setItemLabelGenerator(Location::toString);
        cb.setPlaceholder("type suburb name");
        cb.setItems(l.suburbs().stream().sorted());
        cb.setClearButtonVisible(true);

        mini.add(cb);
        mini.setAlignSelf(Alignment.START);
        mini.setSpacing(true);

        mini.add(new Div());
        mini.add("These are polling places, but most suburbs should be represented, if not choose something close");

        Div body = new Div();
        body.setText("Nothing selected yet");

        Grid<Candidate> candidates = new Grid<>(Candidate.class);
        candidates.addColumn(Candidate::name).setHeader("Name");
        candidates.addColumn(Candidate::partyName).setHeader("Party");
        candidates.addColumn(Candidate::votes).setHeader("Votes");
        candidates.addColumn(c -> String.format("%.2f", (float) c.votes() / (float) _totalVotes * 100.0)).setHeader("Percentage");
        candidates.setVisible(false);

        Button button = new Button("Show Electorate");
        button.addClickListener(event -> {
            body.setText("No search results");
            if (cb.getValue() != null) {
                candidates.setVisible(false);

                String text = String.valueOf(cb.getValue());
                Notification.show(text);

                Location electorate = l.electorateFor(cb.getValue());

                Seat seat = e.contest(electorate);
                body.setText("The suburb " + text + " is in the electorate of " + electorate.toString());
                candidates.setItems(seat.candidates());
                _totalVotes = seat.totalVotes();
                candidates.getColumns()
                        .forEach(c -> c.setAutoWidth(true));
                candidates.recalculateColumnWidths();
                candidates.setVisible(true);
            }
        });

        panel.add(mini);
        panel.add(button);

        panel.add(body);
        panel.add(candidates);
        return panel;
    }

    private VerticalLayout searchByCandidatePanel(Election e) {
        VerticalLayout panel = new VerticalLayout();
        HorizontalLayout mini = new HorizontalLayout();

        ComboBox<Candidate> cb = new ComboBox<>();
        cb.setItemLabelGenerator(Candidate::name);
        cb.setPlaceholder("type candidate name");
        cb.setItems(e.candidates());
        cb.setClearButtonVisible(true);

        mini.add(cb);
        mini.add(new Div());
        mini.add("House of Representatives candidates only");
        mini.setAlignSelf(Alignment.START);
        mini.setSpacing(true);

        Div body = new Div();
        body.setText("Nothing selected yet");

        Grid<Candidate> candidates = new Grid<>(Candidate.class);
        candidates.addColumn(Candidate::name).setHeader("Name");
        candidates.addColumn(Candidate::partyName).setHeader("Party");
        candidates.addColumn(Candidate::votes).setHeader("Votes");
        candidates.addColumn(c -> String.format("%.2f", (float) c.votes() / (float) _totalVotes * 100.0)).setHeader("Percentage");
        candidates.setVisible(false);

        Button button = new Button("Show Electorate");
        button.addClickListener(event -> {
            body.setText("No search results");
            if (cb.getValue() != null) {
                candidates.setVisible(false);

                String text = cb.getValue().name();
                Notification.show(text);

                Location electorate = cb.getValue().electorate();

                Seat seat = e.contest(electorate);
                body.setText("The candidate " + text + " is in the electorate of " + electorate.toString());

                candidates.setItems(seat.candidates());
                _totalVotes = seat.totalVotes();
                candidates.getColumns()
                        .forEach(c -> c.setAutoWidth(true));
                candidates.recalculateColumnWidths();
                candidates.setVisible(true);
            }
        });

        panel.add(mini);
        panel.add(button);

        panel.add(body);
        panel.add(candidates);
        return panel;
    }

}
