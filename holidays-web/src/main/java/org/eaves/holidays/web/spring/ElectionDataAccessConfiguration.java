package org.eaves.holidays.web.spring;

import org.eaves.holidays.web.ElectionApplicationData;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.annotation.ApplicationScope;

@Configuration
public class ElectionDataAccessConfiguration {
    @Bean
    @ApplicationScope(proxyMode = ScopedProxyMode.INTERFACES)
    public ElectionApplicationData applicationContext() {
        return new ElectionApplicationData();
    }
}
