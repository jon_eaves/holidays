Understanding the voting preferences of a location
based on the first preferences of the residents.

This can be used to create a voting sentiment.

###Commands

#### Searching
* locations	(electorateMatch)
* electorate	(suburb, state)
* suburbs	(suburbMatch)
* contest	(suburb, state)
* parties	()
* where (nameMatch)


#### Scoring
* weight (partyId, weight) // adds a weight
* weight (partyId) // removes a weight
* weights () // shows all parties with weights
* score	(suburb, state)
* top (count)
* bottom (count)

* quit	()

### CLI Usage

    > s taroona
    TAROONA::TAS
    > e TAROONA TAS
    CLARK::TAS
    > c CLARK TAS
    Andrew WILKIE : 33761 (48.82) : Independent
    Ben McGREGOR : 13641 (19.73) : Australian Labor Party
    Amanda-Sue MARKHAM : 11719 (16.95) : Liberal
    Juniper SHAW : 6458 (9.34) : The Greens
    Jim STARKEY : 1882 (2.72) : United Australia Party
    Informal Informal : 1689 (2.44) : Informal
    
        










