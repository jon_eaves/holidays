package org.eaves.holidays.cli;

import com.budhash.cliche.Command;
import com.budhash.cliche.ShellFactory;
import org.eaves.holidays.data.*;
import org.eaves.holidays.output.ElectionStringOutputFormatter;
import org.eaves.holidays.scoring.ScoringWeightByParty;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class Holidays {
    private static final String RESULTS = "HouseFirstPrefsByCandidateByVoteTypeDownload-24310.csv";
    private static final String LOCATIONS = "GeneralPollingPlacesDownload-24310.csv";
    private final LocationIndex _locations;
    private final Election _election;
    private final ScoringWeightByParty _scoringWeights;
    private final ElectionStringOutputFormatter _stringOutput;

    public static void main(String[] args) throws IOException {
        ElectionResults r = new CSVNationalElectionResultsReader(RESULTS, "Federal 2019");
        LocationsInElectoratesReader lr = new LocationsInElectoratesReader(LOCATIONS);

        Election e = r.load();
        LocationIndex locations = lr.load();

        System.out.println("Starting Holiday choice CLI");

        ShellFactory.createConsoleShell(">> ", "", new Holidays(e, locations))
                .commandLoop();
    }

    private Holidays(Election e, LocationIndex locations) {
        _election = e;
        _locations = locations;
        _stringOutput = new ElectionStringOutputFormatter(e, locations);
        _scoringWeights = new ScoringWeightByParty(new HashMap<>(), 0);
    }

    @Command
    public String weight(String partyId, int scoreWeight) {
        _scoringWeights.update(partyId, scoreWeight);
        return partyId + " " + _scoringWeights.find(partyId);
    }

    @Command
    public String candidates(String nameMatching) {
        return _stringOutput.candidates(nameMatching);
    }
    
    @Command
    public String weight(String partyId) {
        _scoringWeights.remove(partyId);
        return partyId + " removed";
    }

    @Command
    public String weights() {
        Map<String, Integer> w = _scoringWeights.weights();
        return w.keySet().stream()
                .map(key -> key + " :: " + w.get(key))
                .collect(Collectors.joining("\n"));
    }

    @Command
    public String electorate(String suburb, String state) {
        Location polling = new Location(suburb, state);
        Location electorate = _locations.electorateFor(polling);
        return Objects.requireNonNullElse(electorate, Location.EMPTY).id();
    }

    @Command
    public String suburbs(String someMatch) {
        return _stringOutput.suburbs(someMatch);
    }

    @Command
    public String contest(String suburb, String state) {
        Location l = new Location(suburb, state);
        return _election.contest(Objects.requireNonNullElse(l, Location.EMPTY)).toString();
    }

    @Command
    public String parties() {
        return _stringOutput.parties();
    }

    @Command
    public String top(int number) {
        Scored[] s;
        s = _election.score(_scoringWeights.scoreWeights(), 0);

        StringBuilder rv = new StringBuilder();

        int size = s.length;

        for (int i = 0; i < (Math.min(number, size)); i++) {
            Scored location = s[i];
            String result = String.format("%d. %s\n", i + 1, location.toString());
            rv.append(result);
        }
        return rv.toString();
    }

    @Command
    public String bottom(int number) {
        Scored[] s;
        s = _election.score(_scoringWeights.scoreWeights(), 0);

        StringBuilder rv = new StringBuilder();

        int size = s.length;
        int start = Math.max((size - number), 0);

        for (int i = start; i < size; i++) {
            Scored location = s[i];
            String result = String.format("%d. %s\n", i + 1, location.toString());
            rv.append(result);
        }
        return rv.toString();

    }

    @Command
    public String score(String suburb, String state) {
        Scored[] s;
        s = _election.score(_scoringWeights.scoreWeights(), 0);

        int highest = s[0].score();
        int last = s.length - 1;
        int lowest = s[s.length - 1].score();
        Location l = new Location(suburb, state);
        int rank = -1;
        Scored scored = s[0];

        for (int i = 0; i <= last; i++) {
            scored = s[i];
            if (scored.matches(l)) {
                rank = i;
                break;
            }
        }

        return String.format("Score: %s\n\nRank: %d/%d\nHighest: %d\nLowest: %d", scored.toString(), rank + 1, s.length, highest, lowest);
    }

    @Command
    public String locations(String someMatch) {
        return String.join("\n", _stringOutput.locations(someMatch.toUpperCase()));
    }

    @Command
    public void quit() {
        System.exit(0);
    }
}
