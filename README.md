#holidays

Application to evaluate the voting patterns of electorates to 
determine if I want to support them with my dollars.

## holidays-core
Contains the domain classes

## holidays-cli
Contains a CLI implementation to access the domain

## holidays-web
Contains a Vaadin based web front end useful for deploying
to AWS or similar.

# Development

    cd /holidays/holidays-web
    mvn com.github.eirslett:frontend-maven-plugin:1.7.6:install-node-and-npm -DnodeVersion="v12.13.0"
    cd /holidays
    mvn clean install
    cd holidays-web
    ... do stuff ..
    mvn package

Without the `mvn install` the `holidays-web` package will be unable
to locate the `holidays-core` required jar file.
    
# Packaging

    cd /holidays
    mvn clean install package -Pproduction 
    
# Running

    java -jar holidays.jar --spring.profiles.active=production







