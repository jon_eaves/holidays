package org.eaves.holidays.output;

import org.eaves.holidays.data.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class ElectionStringOutputFormatter {
    private final Election _election;
    private final LocationIndex _locations;

    public ElectionStringOutputFormatter(Election e, LocationIndex l) {
        _election = e;
        _locations = l;
    }

    public String parties() {
        Collection<Party> p = _election.parties();
        return p.stream()
                .map(Party::toString)
                .collect(Collectors.joining("\n"));
    }

    public String[] locations(String someMatch) {
        Location[] l = _election.locations(someMatch);
        return Arrays.stream(l).map(Location::toString).toArray(String[]::new);
    }
    
    public String suburbs(String someMatch) {
        Collection<Location> suburbs = _locations.findSuburbMatching(someMatch);

        return suburbs.stream()
                .map(Location::id)
                .collect(Collectors.joining("\n"));

    }

    public String candidates(String match) {
        Collection<Candidate> candidates = _election.candidates();
        
        String upperMatch = match.toUpperCase();
        
        return candidates.stream()
                .filter( c -> c.name().toUpperCase().contains(upperMatch) )
                .sorted()
                .map(Candidate::toString)
                .collect(Collectors.joining("\n"));
    }
}
