package org.eaves.holidays.data;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LocationsInElectoratesReader {
    private final String _resultsFile;

    public LocationsInElectoratesReader(String resultsFile) {
        _resultsFile = resultsFile;
    }

    public LocationIndex load() {
        List<String[]> entries = read(_resultsFile);
        LocationIndex locations = new LocationIndex();

        for (String[] entry : entries) {
            // System.out.println("Size : " + line.length);
            // System.out.println(Arrays.toString(line));

            if (entry[0].length() == 0 || entry[0].equals("State") || entry.length < 11 ||
                    entry[2].length() == 0 || entry[10].length() == 0 || entry[11].length() == 0) {
                // System.out.printf("Error in line :: %s\n", line);
                continue;
            }

            String state = entry[0];
            String division = entry[2];

            Location l = new Location(division, state);

            String pollingSuburb = entry[10];
            String pollingState = entry[11];

            Location p = new Location(pollingSuburb, pollingState);

            // polling location -> electorate
            locations.addPollingToElectorate(p, l);
        }

        return locations;
    }

    private List<String[]> read(String name) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(name);
        // TODO null processing / exception handling
        Reader r = new InputStreamReader(Objects.requireNonNull(is));

        CSVReader csv = new CSVReader(r);

        List<String[]> entries = new ArrayList<>();
        try {
            entries = csv.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return entries;
    }
}
