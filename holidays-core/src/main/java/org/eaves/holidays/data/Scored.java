package org.eaves.holidays.data;

public class Scored implements Comparable {

    private final int _score;
    private final Seat _seat;
    private final String _representation;

    public Scored(int score, Seat seat) {
        _score = score;
        _seat = seat;
        _representation = _seat.name() + " : " + _score;
    }

    @Override
    public int compareTo(Object o) {
        Scored other = (Scored) o;
        int rv = 0;
        if (_score > other._score) {
            rv = -1;
        } else if (_score < other._score) {
            rv = 1;
        }

        return rv;
    }

    public String toString() {
        return _representation;
    }

    public int score() {
        return _score;
    }
    
    public String seat() {
        return _seat.name();
    }
    
    public Location location() {
        return _seat.location();
    }

    public boolean matches(Location location) {
        return _seat.name().equals(location.id());
    }
}
