package org.eaves.holidays.data;

public class Party {
    private String _abbrev;
    private String _name;
    private String _toString;

    public Party(String abbrev, String name) {
        _abbrev = abbrev;
        _name = name;

        _toString = "(" + _abbrev + ") " + _name;
    }

    public String abbreviation() {
        return _abbrev;
    }

    public String name() {
        return _name;
    }

    public String toString() {
        return _toString;
    }
}
