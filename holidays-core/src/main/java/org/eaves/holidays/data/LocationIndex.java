package org.eaves.holidays.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LocationIndex {
    private Map<Location, Location> _locations;

    public LocationIndex() {
        _locations = new HashMap<>();
    }

    public void addPollingToElectorate(Location polling, Location electorate) {
        _locations.put(polling, electorate);
    }

    public Location electorateFor(Location polling) {
        return _locations.get(polling);
    }

    public Collection<Location> suburbs() {
        return _locations.keySet();
    }

    public Collection<Location> findSuburbMatching(String match) {
        Stream<Location> suburbs = suburbs().stream()
                .filter(s -> s.matches(match))
                .sorted();

        return suburbs.collect(Collectors.toList());
    }
    
    public Collection<Location> suburbsInElectorate(Location electorate) {
        return _locations.entrySet().stream()
                .filter(e -> Objects.equals(e.getValue(), electorate))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public Collection<Location> allElectorates() {
        return _locations.values().stream().distinct().collect(Collectors.toList());
    }
}
