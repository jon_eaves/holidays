package org.eaves.holidays.data;

public class Location implements Comparable<Location> {
    public static final Location EMPTY = new Location("EMPTY", "VALUE");
    private final String _locality;
    private final String _id;
    private final String _state;

    public Location(String locality, String state) {
        _locality = locality.toUpperCase();
        _state = state.toUpperCase();

        _id = _locality + "::" + _state;
    }
    
    public String id() {
        return _id;
    }

    public String toString() {
        return _locality + " " + _state;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (!(other instanceof Location)) {
            return false;
        }

        Location o = (Location) other;
        return o._id.equals(_id);
    }

    private volatile int _hashCode;

    @Override
    public int hashCode() {
        int rv = _hashCode;
        if (rv == 0) {
            rv = 17;
            rv = 31 * rv + _id.hashCode();
            _hashCode = rv;
        }
        return rv;
    }

    public boolean matches(String match) {
        return _locality.contains(match.toUpperCase());
    }

    @Override
    public int compareTo(Location other) {
        return _id.compareTo(other._id);
    }  
}
