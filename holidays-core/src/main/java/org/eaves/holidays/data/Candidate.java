package org.eaves.holidays.data;

public class Candidate implements Comparable<Candidate> {
    private final String _id;
    private final String _name;
    private final String _partyId;
    private final String _partyName;
    private final Location _electorate;
    private final int _voteCount;

    public Candidate(String id, String name, String partyId, String partyName, Location electorate, int voteCount) {
        _id = id;
        _name = name;
        _partyId = partyId;
        _partyName = partyName;
        _electorate = electorate;
        _voteCount = voteCount;
    }

    public String id() {
        return _id;
    }

    public String toString(int totalVotes) {
        return _name + " : " + _voteCount + " (" +
                String.format("%.2f", (float) _voteCount / (float) totalVotes * 100.0)
                + ") : " + _partyName;
    }

    public int votes() {
        return _voteCount;
    }

    public String partyId() {
        return _partyId;
    }

    public String name() {
        return _name;
    }

    public String partyName() {
        return _partyName;
    }

    public Location electorate() {
        return _electorate;
    }

    public String toString() {
        return _name + " : " + _voteCount + " : " + _partyName + " : " + _electorate;
    }
    
    @Override
    public int compareTo(Candidate o) {
        return _name.compareTo(o.name());
    }
}
