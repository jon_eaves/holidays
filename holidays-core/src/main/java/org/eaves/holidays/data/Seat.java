package org.eaves.holidays.data;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Seat {

    public static final Seat EMPTY = new Seat(new Location("EMPTY", "EMPTY"));
    private final Location _location;
    private final Map<String, Candidate> _candidates;
    private int _totalVotes;

    public Seat(Location where) {
        _location = where;
        _candidates = new HashMap<>();
        _totalVotes = 0;
    }

    public String name() {
        return _location.id();
    }
    
    public Location location() {
        return _location;
    }

    public void candidate(String id, Candidate c) {
        _candidates.put(id, c);
        _totalVotes += c.votes();
    }

    public Candidate[] candidates() {
        return _candidates.values().stream()
                .sorted(Comparator.comparingInt(Candidate::votes).reversed())
                .toArray(Candidate[]::new);
    }

    public int totalVotes() {
        return _totalVotes;
    }

    public String toString() {
        return Arrays.stream(candidates())
                .map(cx -> cx.toString(totalVotes()))
                .collect(Collectors.joining("\n"));
    }
}
