package org.eaves.holidays.data;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CSVNationalElectionResultsReader implements ElectionResults {
    private final String _resultsFile;
    private final String _title;

    public CSVNationalElectionResultsReader(String resultsFile, String title) {
        _resultsFile = resultsFile;
        _title = title;
    }

    @Override
    public Election load() {
        Election e = new Election(_title);
        List<String[]> entries = read(_resultsFile);

        for (String[] entry : entries) {
            // System.out.println("Size : " + line.length);
            // System.out.println(Arrays.toString(line));

            if (entry[0].length() == 0 || entry[0].equals("StateAb")) {
                // System.out.printf("Error in line :: %s\n", line);
                continue;
            }
            String state = entry[0];
            String division = entry[2];

            Location l = new Location(division, state);

            String candidateId = entry[3];
            String candidateName = entry[5] + " " + entry[4];
            String partyAb = entry[9];
            String partyName = entry[10];
            if (partyName.equals("Informal")) {
                partyAb = "INF";
            }
            if (partyAb.equals("NAFD")) {
                partyName = "Non-affiliated";
            }

            String votes = entry[16];
            int voteCount = Integer.parseInt(votes);

            Candidate c = new Candidate(candidateId, candidateName, partyAb, partyName, l, voteCount);
            e.candidate(l, c);
        }

        return e;
    }

    private List<String[]> read(String name) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(name);
        // TODO null processing / exception handling
        Reader r = new InputStreamReader(Objects.requireNonNull(is));

        CSVReader csv = new CSVReader(r);

        List<String[]> entries = new ArrayList<>();
        try {
            entries = csv.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return entries;
    }
}
