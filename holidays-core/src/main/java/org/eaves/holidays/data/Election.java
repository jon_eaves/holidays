package org.eaves.holidays.data;

import org.eaves.holidays.scoring.Scoring;

import java.util.*;
import java.util.stream.Collectors;

public class Election {
    private final Map<Location, Seat> _locations;
    private final Map<String, Party> _parties;
    private final Set<Candidate> _candidates;
    private final String _name;

    public Election(String name) {
        _name = name;
        _locations = new HashMap<>();
        _parties = new TreeMap<>();
        _candidates = new HashSet<>();
    }

    public void candidate(Location location, Candidate c) {
        Seat electorate = _locations.get(location);

        if (electorate == null) {
            electorate = new Seat(location);
            _locations.put(location, electorate);
        }

        electorate.candidate(c.id(), c);
        _candidates.add(c);

        if (!_parties.containsKey(c.partyId())) {
            Party p = new Party(c.partyId(), c.partyName());
            _parties.put(c.partyId(), p);
        }
    }

    public Seat contest(Location seat) {
        return Objects.requireNonNullElse(_locations.get(seat), Seat.EMPTY);
    }

    public Location[] locations(String match) {
        return _locations.values().stream().filter(loc -> loc.name().contains(match))
                .map(Seat::location)
                .sorted()
                .toArray(Location[]::new);
    }

    public Scored[] score(Scoring scorer, int threshold) {
        return _locations.values().stream().filter(loc -> scorer.score(loc) > threshold)
                .map(loc -> new Scored(scorer.score(loc), loc))
                .sorted()
                .toArray(Scored[]::new);
    }

    public Collection<Party> parties() {
        return _parties.values().stream().filter(
                p -> !p.name().equalsIgnoreCase("Informal"))
                .collect(Collectors.toList());
    }

    public String toString() {
        return _name;
    }

    public Collection<Candidate> candidates() {
        return _candidates.stream()
                .sorted()
                .filter(c -> !c.partyName().equalsIgnoreCase("Informal"))
                .collect(Collectors.toList());
    }
}
