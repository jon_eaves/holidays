package org.eaves.holidays.data;

public interface ElectionResults {
    Election load();
}
