package org.eaves.holidays.spike;

import com.opencsv.CSVReader;
import org.eaves.holidays.data.*;
import org.eaves.holidays.scoring.FraserAnningOnlyPartyScoring;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CSVRead {
    public static void main(String[] args) {
        String file = "HouseFirstPrefsByCandidateByVoteTypeDownload-24310.csv";
        ElectionResults r = new CSVNationalElectionResultsReader(file, "Federal 2019");

        Election e = r.load();

        Scored[] ordered = e.score(new FraserAnningOnlyPartyScoring(), 1);

        String rv = Arrays.stream(ordered).map(Scored::toString).collect(Collectors.joining("\n"));

        System.out.println(rv);

        Seat here = e.contest(new Location("COOK", "NSW"));
        System.out.println(here);

    }

    public List<String[]> read(String name) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(name);
        Reader r = new InputStreamReader(Objects.requireNonNull(is));

        CSVReader csv = new CSVReader(r);

        List<String[]> entries = new ArrayList<>();
        try {
            entries = csv.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("LineCount :" + entries.size());

        return entries;
    }
}
