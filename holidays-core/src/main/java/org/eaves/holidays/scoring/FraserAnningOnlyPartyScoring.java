package org.eaves.holidays.scoring;

import org.eaves.holidays.data.Seat;

import java.util.Map;

public class FraserAnningOnlyPartyScoring implements Scoring {

    private final CustomScoring _cs;

    public FraserAnningOnlyPartyScoring() {
        Map<String, Integer> SCALING = Map.of("FACN", 10);
        _cs = new CustomScoring(SCALING, 0);
    }

    @Override
    public int score(Seat electorate) {
        return _cs.score(electorate);
    }

}
