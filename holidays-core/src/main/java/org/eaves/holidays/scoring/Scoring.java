package org.eaves.holidays.scoring;

import org.eaves.holidays.data.Seat;

public interface Scoring {

    int score(Seat electorate);

}
