package org.eaves.holidays.scoring;

import java.util.HashMap;
import java.util.Map;

public class ScoringWeightByParty {

    private Map<String, Integer> _partyWeights;
    private int _defaultWeight;

    public ScoringWeightByParty(Map<String, Integer> pw, int defaultWeight) {
        _partyWeights = new HashMap<>(pw);
        _defaultWeight = defaultWeight;
    }

    public int find(String party) {
        int rv = _defaultWeight;
        String compare = party.toUpperCase();
        if (_partyWeights.containsKey(compare)) {
            rv = _partyWeights.get(compare);
        }

        return rv;
    }

    public Map<String, Integer> weights() {
        return new HashMap<>(_partyWeights);
    }

    public void update(String party, int weight) {
        _partyWeights.put(party.toUpperCase(), weight);
    }

    public void remove(String partyId) {
        _partyWeights.remove(partyId.toUpperCase());
    }

    public Scoring scoreWeights() {
        return new CustomScoring(_partyWeights, _defaultWeight);
    }

}
