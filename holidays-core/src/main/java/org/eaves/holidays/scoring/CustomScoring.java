package org.eaves.holidays.scoring;

import org.eaves.holidays.data.Candidate;
import org.eaves.holidays.data.Seat;

import java.util.HashMap;
import java.util.Map;

public class CustomScoring implements Scoring {

    private final Map<String, Integer> _weights;
    private final int _defaultWeight;

    public CustomScoring(Map<String, Integer> weights, int defaultWeight) {
        _defaultWeight = defaultWeight;
        _weights = weights;
    }

    public CustomScoring() {
        _defaultWeight = 5;
        _weights = new HashMap<>();
    }

    @Override
    public int score(Seat electorate) {
        int totalVotes = electorate.totalVotes();
        int score = 0;

        Candidate[] ticket = electorate.candidates();

        for (Candidate c : ticket) {
            int scale = _defaultWeight;
            String partyId = c.partyId();
            if (_weights.containsKey(partyId)) {
                scale = _weights.get(partyId);
            }

            int votes = c.votes();
            score += calculate(votes, totalVotes, scale);

        }
        return score;
    }

    private int calculate(int votes, int total, int scale) {
        if (scale <= 0) {
            return 0;
        }

        double rv = (votes * 100.00) / total * scale;
        return (int) rv;
    }
}
