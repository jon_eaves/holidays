package org.eaves.holidays.scoring;

import org.eaves.holidays.data.Seat;

import java.util.Map;

public class RacistsRUsScoring implements Scoring {

    private CustomScoring _cs;

    public RacistsRUsScoring() {
        Map<String, Integer> SCALING = Map.of("ON", 9, "FACN", 10, "LP", 6,
                "SPP", 9, "GRN", 3, "ALP", 4, "CLP", 6);
        _cs = new CustomScoring(SCALING, 5);
    }

    @Override
    public int score(Seat electorate) {
        return _cs.score(electorate);
    }
}
