package org.eaves.holidays;

import org.eaves.holidays.data.*;
import org.eaves.holidays.scoring.ScoringWeightByParty;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class HolidaysTest {
    private LocationIndex _locations;
    private Election _election;

    public HolidaysTest() {
        String RESULTS = "HouseFirstPrefsByCandidateByVoteTypeDownload-24310.csv";
        String LOCATIONS = "GeneralPollingPlacesDownload-24310.csv";

        ElectionResults r = new CSVNationalElectionResultsReader(RESULTS, "Federal 2019");
        LocationsInElectoratesReader lr = new LocationsInElectoratesReader(LOCATIONS);

        Election e = r.load();
        LocationIndex locations = lr.load();

        _election = e;
        _locations = locations;
    }

    @Test
    public void testElectorates() {
        Location l = new Location("ESSENDON", "VIC");
        Location e1 = _locations.electorateFor(l);

        String e = e1.id();
        assertNotNull(e);
    }

    @Test
    public void testAllSuburbsInElectorate() {
        Location[] locations = _locations.suburbs().toArray(new Location[0]);
        for (Location l : locations) {
            // System.out.printf(":: [%s] \n", pollingPlace.id());
            String e = _locations.electorateFor(l).id();
            assertNotNull(e);
        }
    }

    @Test
    public void testElectoratesHaveContests() {
        Location[] locations = _locations.allElectorates().toArray(new Location[0]);

        for (Location l : locations) {
            // System.out.printf("Seat :: %s \n", l.id());
            Seat s = _election.contest(l);
            assertNotNull(s);
        }
    }

    @Test
    public void testScoringWeights() {
        Map<String, Integer> weights = Map.of("ON", 9);
        ScoringWeightByParty w = new ScoringWeightByParty(weights, 5);
        assertEquals(5, w.find("LNP"));  // default value
        w.update("LNP", 10);
        assertEquals(10, w.find("LNP"));
        assertEquals(9, w.find("ON"));
    }

}
